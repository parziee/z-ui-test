# ZUiTest

This application was made thinking in handle big stuff, please enjoy it!!

This application was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.12.

## Deployment

This application was deployed with `now`, you can check it in: https://z-ui-test.plclavijop.now.sh

## Dependencies

To start using this project you need to install the following dependencies:

> npm install
- [Node JS _Use: v12.10.*_](https://nodejs.org/es/)
- [@angular-cli:8.3.*_](https://github.com/angular/angular-cli)
- npm _Use: v6.10.*_

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests with Jasmin via [Karma](https://karma-runner.github.io).

## Style

I choose SCSS because I feel comfortable with curly braces {}

## Data

This is a 100% frontend application, uses a JSON file (candidate.json) to populate data in the view, also uses LocaStorage to persist it.