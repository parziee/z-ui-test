import { NgModule } from '@angular/core';
import { CandidateService } from '../../services/candidate.service';
import { HowItWorksRoutingModule } from './how-it-wotks.routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HowItWotksComponent } from './how-it-works.component';

@NgModule({
    declarations: [
        HowItWotksComponent
    ],
    imports: [
        HowItWorksRoutingModule,
        SharedModule,
    ],
    providers: [
        CandidateService
    ],
    bootstrap: []
})

export class HowItWorksModule { }
