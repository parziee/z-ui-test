import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HowItWotksComponent } from './how-it-works.component';

const routes: Routes = [
    { path: '', component: HowItWotksComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HowItWorksRoutingModule { }
