import { NgModule } from '@angular/core';
import { CandidateService } from '../../services/candidate.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { PastTrialsComponent } from './past-trials.component';
import { PastTrialsRoutingModule } from './past-trials.routing.module';

@NgModule({
    declarations: [
        PastTrialsComponent
    ],
    imports: [
        PastTrialsRoutingModule,
        SharedModule,
    ],
    providers: [
        CandidateService
    ],
    bootstrap: []
})

export class PastTrialsModule { }
