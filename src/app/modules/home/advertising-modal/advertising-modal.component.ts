import { Component } from '@angular/core';

@Component({
    selector: 'app-advertising-modal',
    templateUrl: './advertising-modal.component.html',
    styleUrls: ['./advertising-modal.component.scss']
})
export class AdvertisingModalComponent {

    public isModalClosed: boolean = false;

    constructor(
    ) { }

    closeModal() {
        this.isModalClosed = true;
    }

}
