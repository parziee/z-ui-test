import { NgModule } from '@angular/core';
import { CandidateService } from '../../services/candidate.service';
import { HeaderComponent } from './header/header.component';
import { HeroComponent } from './header/hero/hero.component';
import { ClosingInBarComponent } from './header/closing-in-bar/closing-in-bar.component';
import { AdvertisingModalComponent } from './advertising-modal/advertising-modal.component';
import { HomeComponent } from './home.component';
import { VotesComponent } from './votes/votes.component';
import { CandidateComponent } from './votes/candidate/candidate.component';
import { SubmitBannerComponent } from './submit-banner/submit-banner.component';
import { HomeRoutingModule } from './home.routing.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        HomeComponent,
        HeaderComponent,
        HeroComponent,
        ClosingInBarComponent,
        AdvertisingModalComponent,
        VotesComponent,
        CandidateComponent,
        SubmitBannerComponent,
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        SharedModule,
    ],
    providers: [
        CandidateService
    ]
})

export class HomeModule { }
