import { Component, OnInit, Input } from '@angular/core';
import { DateUtils } from 'src/app/utils/DateUtils';

@Component({
    selector: 'app-closing-in-bar',
    templateUrl: './closing-in-bar.component.html',
    styleUrls: ['./closing-in-bar.component.scss']
})
export class ClosingInBarComponent implements OnInit {

    @Input() daysPassed: number;
    @Input() voteDays: number;

    public daysLeft: number;
    public leftWidthPercent: string;
    public rightWidthPercent: string;

    constructor() { }

    ngOnInit() {
        this.daysPassed = this.daysPassed || new Date().getDate();
        this.voteDays = this.voteDays || DateUtils.daysInThisMonth();
        this.daysLeft = this.calculateDaysLeft();
    }

    private calculateDaysLeft() {
        let daysInPercentage = (this.daysPassed * 100) / this.voteDays;
        daysInPercentage = daysInPercentage < 10 ? 10 : (daysInPercentage > 90 ? 90 : daysInPercentage);
        this.leftWidthPercent = `${daysInPercentage}%`;
        this.rightWidthPercent = `${100 - daysInPercentage}%`;

        return this.voteDays - this.daysPassed;
    }

}
