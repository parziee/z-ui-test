import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ClosingInBarComponent } from './closing-in-bar.component';

describe('ClosingInBarComponent', () => {

    let component: ClosingInBarComponent;
    let fixture: ComponentFixture<ClosingInBarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ClosingInBarComponent,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule
            ]
        });
        TestBed.compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ClosingInBarComponent);
        component = fixture.componentInstance;
    });

    it('should create component instance', () => {
        fixture.detectChanges();

        expect(component).toBeTruthy();
    });

    it('should execute component ngOnInit and return days left and widths in percentage', () => {
        component.daysPassed = 6;
        component.voteDays = 30;
        fixture.detectChanges();
        component.ngOnInit();

        expect(component.daysLeft).toBe(24);
        expect(component.leftWidthPercent).toBe('20%');
        expect(component.rightWidthPercent).toBe('80%');
    });


});
