
import { Component, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { DateUtils } from 'src/app/utils/DateUtils';
import { Candidate } from 'src/app/entities/Candidate';

@Component({
    selector: 'app-candidate',
    templateUrl: './candidate.component.html',
    styleUrls: ['./candidate.component.scss']
})
export class CandidateComponent implements OnChanges {

    @Input() candidate: Candidate;
    @Output() onCandidateChange: EventEmitter<any> = new EventEmitter<any>();

    public thumbsUpPorcentage: string;
    public thumbsDownPorcentage: string;
    public voteUp: boolean = false;
    public voteDown: boolean = false;
    public showVoteAgain: boolean;
    public isWinningVotes: boolean;
    public formattedDate: string;

    constructor(
    ) {
        this.showVoteAgain = false;
    }

    ngOnChanges() {
        if (this.candidate && this.candidate.id) {
            this.refresh();
        }
    }

    private refresh() {
        this.calculateVotes();
        this.formattedDate = DateUtils.timeAgo(this.candidate.date);
    }

    private calculateVotes() {
        const totalVotes = this.candidate.thumbs_up + this.candidate.thumbs_down;
        this.isWinningVotes = this.candidate.thumbs_up >= this.candidate.thumbs_down;
        this.thumbsUpPorcentage = `${Math.round((this.candidate.thumbs_up * 100) / totalVotes)}%`;
        this.thumbsDownPorcentage = `${Math.round((this.candidate.thumbs_down * 100) / totalVotes)}%`;
    }

    check(vote) {
        this.voteUp = vote;
        this.voteDown = !vote;
    }

    persistCandidate() {
        const now = new Date();

        if (this.voteUp) {
            this.candidate.thumbs_up += 1;
        } else {
            this.candidate.thumbs_down += 1;
        }
        this.candidate.date = now;
        this.onCandidateChange.emit(this.candidate);
        this.showVoteAgain = true;
        this.refresh();
    }

    hideVoteAgain() {
        this.showVoteAgain = false;
    }

    get canVote() {
        return this.voteUp || this.voteDown;
    }

}
