import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { CandidateComponent } from './candidate.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MOCK_CANDIDATE as CANDIDATE } from 'src/app/utils/candidate';

describe('CandidateComponent', () => {

    let component: CandidateComponent;
    let fixture: ComponentFixture<CandidateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                CandidateComponent,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule
            ]
        });
        TestBed.compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CandidateComponent);
        component = fixture.componentInstance;
    });

    it('should create component instance', () => {
        component.candidate = CANDIDATE;
        fixture.detectChanges();

        expect(component).toBeTruthy();
    });

    it('should execute component onChanges with isWinningVotes false and percentages equals to 40%, 60%', () => {
        component.candidate = CANDIDATE;
        fixture.detectChanges();
        component.ngOnChanges();

        expect(component.isWinningVotes).toBe(false);
        expect(component.thumbsUpPorcentage).toBe('40%');
        expect(component.thumbsDownPorcentage).toBe('60%');
    });

    it('should execute component check (click thumbs up)', () => {
        component.candidate = CANDIDATE;
        fixture.detectChanges();
        component.check(true);

        expect(component.voteUp).toBe(true);
        expect(component.voteDown).toBe(false);
    });

    it('should execute component persistCandidate and increment thumbs up in 1', async () => {
        component.candidate = CANDIDATE;
        fixture.detectChanges();
        component.check(true);
        await component.persistCandidate();

        expect(component.candidate.thumbs_up).toBe(3);
        expect(component.showVoteAgain).toBe(true);
    });

});
