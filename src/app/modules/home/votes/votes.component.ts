
import { Component, OnInit } from '@angular/core';
import { CandidateService } from 'src/app/services/candidate.service';
import { Candidate } from 'src/app/entities/Candidate';

@Component({
    selector: 'app-votes',
    templateUrl: './votes.component.html',
    styleUrls: ['./votes.component.scss']
})
export class VotesComponent implements OnInit {

    public candidates: Array<Candidate> = [];

    constructor(
        private candidateService: CandidateService,
    ) { }

    ngOnInit() {
        this.validateExistingData();
    }

    private validateExistingData() {
        const storagedCandidates = JSON.parse(localStorage.getItem('candidates'));
        if (storagedCandidates && storagedCandidates.length) {
            this.candidates = storagedCandidates;
        } else {
            this.getAllCandidates();
        }
    }

    private getAllCandidates() {
        this.candidateService.getAllCandidates().subscribe(({ data, success }) => {
            if (success) {

                this.candidates = data as Array<Candidate>;
            }
        });
    }

    persistCandidate(candidate) {
        this.candidates.map(o => o.id === candidate.id ? o = candidate : o);
        localStorage.setItem('candidates', JSON.stringify(this.candidates));
    }

}
