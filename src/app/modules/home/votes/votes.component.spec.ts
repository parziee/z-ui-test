import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { defer } from 'rxjs';
import { CandidateService } from 'src/app/services/candidate.service';
import { RouterTestingModule } from '@angular/router/testing';
import { VotesComponent } from './votes.component';
import { CandidateComponent } from './candidate/candidate.component';
import { MOCK_CANDIDATE as CANDIDATE } from 'src/app/utils/candidate';

const callResolve = true;

export function fakeAsyncResponse<T>(data: T) {
    return defer(() => Promise.resolve(data));
}

export function fakeAsyncResponseError<T>(data: T) {
    return defer(() => Promise.reject(data));
}

class MockCandidateService {
    getAllCandidates() {
        if (callResolve) {
            return fakeAsyncResponse({
                data: [CANDIDATE], success: true
            });
        } else {
            return fakeAsyncResponseError({ data: [], success: false });
        }
    }
}

describe('VotesComponent', () => {

    let component: VotesComponent;
    let fixture: ComponentFixture<VotesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                VotesComponent,
                CandidateComponent,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ]
        });
        TestBed.overrideComponent(VotesComponent, {
            set: {
                providers: [
                    { provide: CandidateService, useClass: MockCandidateService },
                ],
            }
        });
        TestBed.compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VotesComponent);
        component = fixture.componentInstance;
    });

    it('should create component instance', () => {
        fixture.detectChanges();

        expect(component).toBeTruthy();
    });

    it('Should create component instance ngOnInit and get all Candidates', async () => {
        fixture.detectChanges();
        await component.ngOnInit();

        expect(component.candidates).toEqual([CANDIDATE]);
    });

});
