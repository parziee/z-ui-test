import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    /* { path: 'login', component: LoginComponent },*/
    {
        path: '', redirectTo: 'home', pathMatch: 'full'
    },
    {
        path: 'home', loadChildren: () => import('src/app/modules/home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'how-it-works', loadChildren: () => import('src/app/modules/how-it-works/how-it-works.module').then(m => m.HowItWorksModule)
    },
    {
        path: 'past-trials', loadChildren: () => import('src/app/modules/past-trials/past-trials.module').then(m => m.PastTrialsModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
