import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class CandidateService {

    constructor(
        private httpClient: HttpClient
    ) { }

    getAllCandidates(): Observable<any> {
        return this.httpClient.get('assets/json/candidates.json');
    }

}
