export class DateUtils {

    static timeAgo(dateParam) {

        if (!dateParam) {
            return null;
        }
        const correctTimeZone = dateParam.toLocaleString('en-US', {timeZone: 'America/New_York'});
        const date: any = new Date(correctTimeZone);
        const today: any = new Date();
        const seconds = Math.round((today - date) / 1000);
        const minutes = Math.round(seconds / 60);
        const hours = Math.round(minutes / 60);
        const days = Math.round(hours / 24);
        const months = Math.round(days / 30);
        const years = Math.round(months / 12);

        if (seconds < 5) {
            return 'now';
        } else if (seconds < 60) {
            return `${seconds} seconds ago`;
        } else if (seconds < 90) {
            return 'about a minute ago';
        } else if (minutes < 60) {
            return `${minutes} minutes ago`;
        } else if (hours < 24) {
            return `${hours} hours ago`;
        } else if (days < 30) {
            return `${days} days ago`;
        } else if (months < 12) {
            return `${months} months ago`;
        } else {
            return `${years} years ago`;
        }
    }

    static daysInThisMonth() {
        const now = new Date();

        return new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
    }

}
