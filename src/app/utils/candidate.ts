export const MOCK_CANDIDATE = {
    id: 182,
    name: 'Parzifal',
    img_path: '',
    date: new Date(),
    topic: 'Software',
    description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
    thumbs_up: 2,
    thumbs_down: 3,
};
