import { NgModule } from '@angular/core';
import { NavMenuComponent } from 'src/app/shared/nav-menu/nav-menu.component';
import { FooterComponent } from 'src/app/shared/footer/footer.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        NavMenuComponent,
        FooterComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
    ],
    exports: [
        NavMenuComponent,
        FooterComponent,
    ]
})

export class SharedModule { }
