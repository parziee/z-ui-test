import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent {

    @ViewChild('topNav', { static: false }) topNav: ElementRef;

    constructor(
    ) { }

    collapse() {
        if (this.topNav.nativeElement.className === 'menu-options') {
            this.topNav.nativeElement.className += ' responsive';
        } else {
            this.topNav.nativeElement.className = 'menu-options';
        }
    }

}
