export class Candidate {
    public id: number;
    public name: string;
    public img_path: string;
    public date: Date;
    public topic: string;
    public description: string;
    public thumbs_up: number;
    public thumbs_down: number;

    public constructor(init?: Candidate) {
        Object.assign(this, init);
    }
}
